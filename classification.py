import numpy as np
from sklearn import metrics
import pandas

import tensorflow.contrib.learn as skflow


filename = 'LabelledData (1).txt'
trainFilename = 'trainData.csv'
testFilename = 'testData.csv'
labelMap = {'who': 0, 'what': 1, 'when': 2, 'affirmation': 3, 'unknown': 4}
distributionRatio = 0.8
iterationCount = 10
testResultFilename = 'predictionResult.csv'
separator = ' ,,, '


def readFile(filename):
    file = open(filename, 'r')
    content = file.read()
    file.close()
    return content

def getRows(content):
    lines = content.split('\n')
    lines.pop();

    questions = []
    labels = []

    for line in lines:
        subPart = line.split(separator)
        question = subPart[0].strip().rstrip()
        question = question.replace('``', '\'\'')
        question = question.replace('\xef\xbf\xbd', '?')
        question = question.replace(',', '')
        questions.append(question)
        labels.append(subPart[1].strip().rstrip())

    return questions, labels

def makeFileWithUniformDistribution(trainFilename, testFilename, questions, labels):
    sentenceMap = {0: [], 1: [], 2: [], 3: [], 4: []}

    for i in range(0, len(questions)):
        sentenceMap[labelMap[labels[i]]].append(questions[i])

    trainFile = open(trainFilename, 'w')
    testFile = open(testFilename, 'w')
    for key in sentenceMap:
        trainingSize = int(round(distributionRatio * len(sentenceMap[key])))
        testSize = len(sentenceMap[key]) - trainingSize

        for i in range(0, trainingSize):
            trainFile.write(str(key) + ',' + sentenceMap[key][i] + '\n')

        for i in range(trainingSize, trainingSize + testSize):
            testFile.write(str(key) + ',' + sentenceMap[key][i] + '\n')

    trainFile.close()
    testFile.close()

def makeFileWithoutUniformDistribution(trainFilename,testFilename,questions,labels):
    trainingSize = int(round(distributionRatio * len(questions)))
    testSize = len(questions) - trainingSize

    file = open(trainFilename, 'w')
    for i in range(0, trainingSize):
        file.write(str(labelMap[labels[i]]) + ',' + questions[i] + '\n')
    file.close()

    file = open(testFilename, 'w')
    for i in range(trainingSize, trainingSize + testSize):
        file.write(str(labelMap[labels[i]]) + ',' + questions[i] + '\n')
    file.close()

def predictUsingML(trainFilename,testFilename):
    train = pandas.read_csv(trainFilename, header=None)
    X_train, y_train = train[1], train[0]
    test = pandas.read_csv(testFilename, header=None)
    X_test, y_test = test[1], test[0]

    ### Process vocabulary

    MAX_DOCUMENT_LENGTH = 20

    vocab_processor = skflow.preprocessing.VocabularyProcessor(MAX_DOCUMENT_LENGTH)
    X_train = np.array(list(vocab_processor.fit_transform(X_train)))
    X_test = np.array(list(vocab_processor.transform(X_test)))

    n_words = len(vocab_processor.vocabulary_)
    print('Total words: %d' % n_words)

    ### Models

    EMBEDDING_SIZE = 50

    # Customized function to transform batched X into embeddings
    def input_op_fn(X):
        # Convert indexes of words into embeddings.
        # This creates embeddings matrix of [n_words, EMBEDDING_SIZE] and then
        # maps word indexes of the sequence into [batch_size, sequence_length,
        # EMBEDDING_SIZE].
        word_vectors = skflow.ops.categorical_variable(X, n_classes=n_words,
                                                       embedding_size=EMBEDDING_SIZE, name='words')
        # Split into list of embedding per word, while removing doc length dim.
        # word_list results to be a list of tensors [batch_size, EMBEDDING_SIZE].
        word_list = skflow.ops.split_squeeze(1, MAX_DOCUMENT_LENGTH, word_vectors)
        return word_list

    # Single direction GRU with a single layer
    classifier = skflow.TensorFlowRNNClassifier(rnn_size=EMBEDDING_SIZE,
                                                n_classes=len(labelMap.keys()), cell_type='gru', input_op_fn=input_op_fn,
                                                num_layers=1, bidirectional=False, sequence_length=None,
                                                steps=100, optimizer='Adam', learning_rate=0.01, continue_training=True)

    # Continously train for 1000 steps & predict on test set.
    for i in range(0, iterationCount):
        classifier.fit(X_train, y_train, logdir='/tmp/tf_examples/word_rnn')
        prediction = classifier.predict(X_test)
        score = metrics.accuracy_score(y_test, prediction)
        print('Accuracy: {0:f}'.format(score))

    return prediction;

def getLabelName(searchValue):
    for key, value in labelMap.iteritems():
        if value == searchValue:
            return key

def savePrediction(testFilename,testPrediction,testResultFilename):
    file = open(testFilename)
    content = file.read()
    file.close()

    lines = content.split('\n')
    lines.pop()

    testResultFile = open(testResultFilename,'w')
    testResultFile.write('Question' + ',' + 'Labeled Category' + ',' + 'Predicted Category' + '\n')

    for i in range(0,len(lines)):
        subPart = lines[i].split(',')
        question = subPart[1]
        labeledCategory = getLabelName(int(subPart[0]))
        predictedCategory = getLabelName(int(testPrediction[i]))

        testResultFile.write(question + ',' + labeledCategory + ',' + predictedCategory + '\n')

        #print question,labeledCategory,predictedCategory

    testResultFile.close()



if __name__ == "__main__":
    content = readFile(filename)
    questions, labels = getRows(content)
    makeFileWithUniformDistribution(trainFilename, testFilename, questions, labels)
    #makeFileWithoutUniformDistribution(trainFilename, testFilename, questions, labels)
    testPrediction = predictUsingML(trainFilename, testFilename)
    savePrediction(testFilename,testPrediction,testResultFilename)



